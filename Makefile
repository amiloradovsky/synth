LDFLAGS += ${GCCFLAGS} -lm -lX11 \
	-L/nix/store/mp00dzlkz20ryzga874iawinc8yd1zh9-x11env/lib
CFLAGS  += ${GCCFLAGS} -Wall -g -O0 \
	-DMATH_H_LOG -DMATH_H_EXP -DLITTLE_ENDIAN_MACHINE \
	-I/nix/store/mp00dzlkz20ryzga874iawinc8yd1zh9-x11env/include

synth : main.o parse.o synth.o complex.o rational.o \
	buffer.o sun_au.o audio.o video.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $?
.o : .c .h
	$(CC) $(CFLAGS) $(LDFLAGS) -c $?

all : synth
