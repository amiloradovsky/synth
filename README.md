A minimalistic command-line driven music synthesizer:
compiles a low-level description(s) of the sound(s) into an audio file,
in the Sun Audio file format. Which, in turn, may be played/imported.
Supports mixing of multiple channels (e.g. bass, middle, treble),
and a mechanism for operation with gammas and tempos.
The available effects include overtones and compression/overdrive/distortion.

Provided that there are GNU `make` and SoX `play` installed in the system.
To listen for some examples, try:

    $ cd synth/
    $ make
    $ cd examples/
    $ make
    $ play *.au

Does not employ a MIDI of any kind: just complex exponential oscillators.
There is also included an audio player for the (Sun Audio) format,
but currently it only works with the OpenBSD's sound interface.