#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>
#include "success.h"
#include "sun_au.h"
#include "buffer.h"
#include "audio.h"

#ifdef OPENBSD
#include "device.h"
#endif

const float capacity = 1.0 ;	/* of buffers, in seconds */

int8_t * buf ;    /* output representation */
double * buf0 ;    /* internal representation */
int idx ;    /* current position in buffer */
u_int count ;    /* total count of samples in each buffer */
u_int size ;    /* bytes per sample per channel */
enum au_encoding enc ;
int file ;	/* Are we writing in regular file? */
int ofd ;	/* output */

FUNC audio_init ( e, r, c, d, f, t )
	enum au_encoding e ;
	int r, c ;
	char * d, * f ;
	enum output_type t ;
FUNC_BEGIN
	idx = 0 ;
	size = au_sample_size_v [ e ];
	count = capacity * r ;
	enc = e ;
	buf = (int8_t *) calloc ( count, size * c );
	buf0 = (double *) calloc ( count, sizeof ( * buf0 ) * c );
#ifdef OPENBSD
	if ( t == FILE_OUTPUT ) {
#endif
		if ( f )
			N ( ( ofd = open ( f, O_WRONLY | O_CREAT | O_TRUNC, 0644 ) ) + 1 )
		else
			ofd = STDOUT_FILENO ;
		/* 0 - because count of samples is unknown */
		Z ( au_header_write ( ofd, e, r, c, 0, d ) )
		file = 1 ;
#ifdef OPENBSD
	} else if ( t == DEVICE_OUTPUT ) {
		Z ( device_set ( WRITE, &ofd, e, r, c ) )
		file = 0 ;
	} else
		ENOUGH
#endif
FUNC_END

FUNC buffer_flush ()
FUNC_BEGIN
	Z ( buffer_from_double ( enc, buf0, buf, idx ) )
	if ( file )
		Z ( au_buffer_write ( ofd, buf, &idx, size ) )
	else
		Z ( buffer_rw ( WRITE, ofd, buf, &idx, size ) )
	idx = 0 ;
FUNC_END

FUNC sample_push ( OUTPUT x )
FUNC_BEGIN
	*( buf0 + idx ) = x ;
	idx ++ ;
	if ( idx == count ) {
		Z ( buffer_flush () )
	};
FUNC_END
