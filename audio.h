typedef float OUTPUT ;

enum output_type { FILE_OUTPUT, DEVICE_OUTPUT };

FUNC audio_init ( enum au_encoding, int, int,
	char *, char *, enum output_type );

FUNC sample_push ( OUTPUT );

FUNC buffer_flush ();
