#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#include "success.h"
#include "sun_au.h"
#include "buffer.h"
#include "device.h"

const char aurec [] = { "aurec" };

int8_t buf [ 8192 ];

void
print_info ( h, d )
	const struct au_header * h ;
	char * d ;
{
	printf ( "Encoding: %d\n", (*h).encoding );
	printf ( "Bytes/sample: %d\n",
		au_sample_size_v [ (*h).encoding ] );
	printf ( "Sample rate: %d\n", (*h).sample_rate );
	printf ( "Channels: %d\n", (*h).channels );
	if ( d )
		printf ( "Description: %s\n", d );
}

FUNC play_file ( char * f )
FUNC_BEGIN
	struct au_header h ;
	int rfd, wfd, t, bps ;
	char * desc ;

	if ( f ) {
		N ( ( rfd = open ( f, O_RDONLY ) ) + 1 )
	} else
		rfd = STDIN_FILENO ;

	Z ( au_header_read ( rfd, &h, &desc ) )
	print_info ( &h, desc );
	Z ( device_set ( WRITE, &wfd, h.encoding,
		h.sample_rate, h.channels ) )

	bps = au_sample_size_v [ h.encoding ];
	do {
		t = sizeof ( buf ) / bps;
	} while (
		! au_buffer_read ( rfd, buf, &t, bps ) &&
		t &&
		! buffer_rw ( WRITE, wfd, buf, &t, bps )
	);
	close ( rfd );
	close ( wfd );
FUNC_END

FUNC record_file ( f, e, r, c, d )
	enum au_encoding e ;
	int r, c ;
	char * d, * f ;
FUNC_BEGIN
	int rfd, wfd, t, bps ;

	if ( f ) {
		N ( ( wfd = open ( f,
				O_WRONLY | O_CREAT | O_TRUNC, 0644 ) ) + 1 )
	} else
		wfd = STDOUT_FILENO ;

	/* 0 - because count of samples is unknown */
	Z ( au_header_write ( wfd, e, r, c, 0, d ) )
	Z ( device_set ( READ, &rfd, e, r, c ) )

	bps = au_sample_size_v [ e ];
	do {
		t = sizeof ( buf ) / bps ;
	} while (
		! buffer_rw ( READ, rfd, buf, &t, bps ) &&
		t &&
		! au_buffer_write ( wfd, buf, &t, bps )
	);
	close ( rfd );
	close ( wfd );
FUNC_END

int
main ( argc, argv )
	int argc ;
	char ** argv ;
FUNC_BEGIN
	enum au_encoding e = LINEAR_16 ;
	int r = 44100, c = 1, write = 0, i ;
	char * d = 0 ;

	if ( strncmp ( basename ( * argv ), aurec, sizeof ( aurec ) ) == 0 )
		write ++ ;

	while ( ( i = getopt ( argc, argv, "e:r:c:d:w" ) ) + 1 )
		if ( i == 'e' )
			if ( * optarg == '1' )
				e = LINEAR_8 ;
			else if ( * optarg == '2' )
				e = LINEAR_16 ;
			else if ( * optarg == '3' )
				e = LINEAR_24 ;
			else if ( * optarg == '4' )
				e = LINEAR_32 ;
			else
				err ( 1, "Only linear encodings supported." );
		else if ( i == 'r' )
			r = atoi ( optarg ) ;
		else if ( i == 'c' )
			c = atoi ( optarg ) ;
		else if ( i == 'd' )
			d = optarg ;
		else if ( i == 'w' )
			write ++ ;
		else
			err ( 1, "usage: %s [-w] [-e <1-4>] \
[-r <integer>] [-c <integet>] \
[-d <string>] [-f <string>]\n", argv[0] );
	argc -= optind ;
	argv += optind ;

	if ( write ) {
		if ( argc ) {
			if ( argc != 1 )
				warn ( "Too many files, rest ignored." );
			Z ( record_file ( * argv, e, r, c, d ) )
		} else
			Z ( record_file ( 0, e, r, c, d ) )
	} else {
		if ( argc ) {
			for ( i = 0 ; i < argc ; i ++ )
				Z ( play_file ( *( argv + i ) ) )
		} else
			Z ( play_file ( 0 ) )
	}
FUNC_END
