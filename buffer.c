#include <string.h>
#include <err.h>
#include <unistd.h>
#include "success.h"
#include "sun_au.h"
#include "buffer.h"

FUNC buffer_revert ( b, c, r )
	int8_t * b ;	/* buffer */
	u_int c ;	/* count of samples */
	u_int r ;	/* resolution (bytes per sample) */
FUNC_BEGIN
	int i ;	/* sample */

	N ( b )
	N ( r )
	if ( r != 1 )	/* speed up */
		for ( i = 0 ; i < c ; i ++ ) {
			int8_t * f ;	/* first byte */
			int8_t * l ;	/* last byte */

			f = b + i * r ;
			l = f + r - 1 ;
			while ( l > f ) {
				int8_t t ;

				t = *f ;
				*f = *l ;
				*l = t ;

				f ++ ;
				l -- ;
			}
		}
FUNC_END

FUNC buffer_rw ( o, f, b, c, r )
	enum read_or_write o ;	/* operation */
	int f ;	/* descriptor */
	int8_t * b ;	/* buffer */
	int * c ;	/* count of samples */
	int r ;	/* bytes per sample */
FUNC_BEGIN
	int t, u, v ;

	u = *c * r ;
	v = 0 ;
	while ( v != u ) {
		/* TODO: need for speed (pointer to function?) */
		if ( o == READ )
			t = read ( f, b + v, u - v );
		else if ( o == WRITE )
			t = write ( f, b + v, u - v );
		else
			ENOUGH
		SUCCESS

		if ( t == 0 || t + 1 == 0 ) {
			*c = v / r ;
			ENOUGH
		}

		v += t ;
	}
FUNC_END

FUNC buffer_from_double ( e, src, dst, n )
	enum au_encoding e ;
	double * src ;
	int8_t * dst ;
	u_int n ;
FUNC_BEGIN
	u_int i, s ;

	s = au_sample_size_v [ e ];
	if ( e == FLOAT )
		for ( i = 0 ; i < n ; i ++ )
			*( ( float * ) dst + i ) = ( float ) *( src + i );
	else if ( e == DOUBLE )
		for ( i = 0 ; i < n ; i ++ )
			*( ( double * ) dst + i ) = ( double ) *( src + i );
	else if (
		( e == LINEAR_8  ) || ( e == FIXED_8  ) ||
		( e == LINEAR_16 ) || ( e == FIXED_16 ) ||
		( e == LINEAR_24 ) || ( e == FIXED_24 ) ||
		( e == LINEAR_32 ) || ( e == FIXED_32 )
	)
		for ( i = 0 ; i < n ; i ++ ) {
			u_int32_t t ;

			t = ( u_int32_t )( *( src + i ) *
				( ( u_int32_t )( - 1 ) >> 1 ) );
			t >>= ( sizeof ( t ) - s ) * 8 ;
#ifdef LITTLE_ENDIAN_MACHINE
			memcpy ( dst + i * s, &t, s );
#else
			memcpy ( dst + i * s, &t + sizeof ( t ) - s, s );
#endif
		}
	else
		ENOUGH
FUNC_END
