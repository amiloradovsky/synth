enum read_or_write { READ, WRITE };

FUNC buffer_revert ( int8_t *, u_int, u_int );

FUNC buffer_rw ( enum read_or_write, int, int8_t *, int *, int );

FUNC buffer_from_double ( enum au_encoding, double *, int8_t *, u_int );
