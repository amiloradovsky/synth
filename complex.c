#if defined MATH_H_LOG || defined MATH_H_EXP
#include <math.h>
#endif
#include "success.h"
#include "complex.h"

inline struct complex
cnew ( REAL a, REAL b )
{
	struct complex r;

	r.re = a;
	r.im = b;

	return r;
}

inline struct complex
csum(struct complex a, struct complex b)
{
	struct complex r;

	r.re = a.re + b.re;
	r.im = a.im + b.im;

	return r;
}

inline struct complex
cmul(struct complex a, struct complex b)
{
	struct complex r;

	r.re = a.re * b.re - a.im * b.im;
	r.im = a.re * b.im + a.im * b.re;

	return r;
}

inline success_t
cnull ( struct complex a )
{
	return ( a.re == 0 && a.im == 0 );
}

inline REAL
cabs2 ( struct complex a )
{
	return ( a.re * a.re + a.im * a.im );
}

/* Below, `MATH_H_EXP` and `MATH_H_LOG` indicate
   the reliance on the standard library functions,
   and not an own implementation of the series.
   Underscores (_) are added to distinguish from
   the standard functions `cexp` and `clog` */

struct complex
cexp_ ( struct complex z )
{
	struct complex r ;
#ifdef MATH_H_EXP
	REAL exp_re = exp ( z.re );

	r.re = exp_re * cos(z.im);
	r.im = exp_re * sin(z.im);
#else
	struct complex t ;
	int i ;

	r = cnew ( 0, 0 );
	t = cnew ( 1, 0 );
	i = 0 ;
	while ( ! cnull ( t ) ) {
		r.re += t.re ;
		r.im += t.im ;
		i ++ ;
		t = cmul ( t, z ); 
		t.re /= i ;
		t.im /= i ;
	};
#endif
	return r ;
}

 /* assuming z != 0 */
struct complex
clog_ ( struct complex z )
{
	struct complex r ;
#ifdef MATH_H_LOG
	r.re = log ( cabs2 ( z ) ) / 2 ;
	r.im = atan2 ( z.im, z.re );
#else
	struct complex s, t ;
	REAL i ;

	/*
	 * log(1 + x) = x - x^2/2 + x^3/3 - ...
	 * z = 1 + x
	 * log(z) = - S (1 - z)^n/n,	(n in N)
	 * log(z) = - log(1/z) = S_n (1 - 1/z)^n/n
	 */
	z.re = 1 - z.re ;
	z.im = 0 - z.im ;
	r = cnew ( 0, 0 );
	s = r ;
	t = z ;
	i = 1 ;
	while ( 0 == 0 ) {
		r.re -= t.re / i ;
		r.im -= t.im / i ;
		if ( s.re == r.re && s.im == r.im )
			break ;
		s.re = r.re ;
		s.im = r.im ;
		i ++ ;
		t = cmul ( t, z );
	};
#endif
	return r ;
}
