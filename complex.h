typedef float REAL ;
struct complex {
	REAL re, im ;	/* real and imaginary part */
};

inline struct complex
cnew(REAL, REAL);

inline struct complex
csum(struct complex, struct complex);

inline struct complex
cmul(struct complex, struct complex);

inline REAL
cabs2 ( struct complex );

inline struct complex
cexp_(struct complex);

inline struct complex
clog_(struct complex);
