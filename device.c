#include <err.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/audioio.h>
#include "success.h"
#include "sun_au.h"
#include "buffer.h"
#include "device.h"

FUNC device_set ( o, f, e, r, c )
	enum read_or_write o ;
	enum au_encoding e ;
	int * f, r, c ;
FUNC_BEGIN
	int fd, flags ;
	audio_info_t ai;
	struct audio_prinfo * t ;

	if ( o == READ ) {
		flags = O_RDONLY ;
		t = &( ai.record );
	} else if ( o == WRITE ) {
		flags = O_WRONLY ;
		t = &( ai.play );
	} else
		ENOUGH
	SUCCESS

	N ( ( fd = open ( "/dev/audio", flags ) ) + 1 )
	N ( ioctl ( fd, AUDIO_GETINFO, &ai ) + 1 )

	(*t).sample_rate = r ;
	(*t).channels = c ;
	N ( (*t).precision = au_sample_size_v [ e ] * 8 )
	if ( e == ULAW )
		(*t).encoding = AUDIO_ENCODING_ULAW ;
	else if ( e == ALAW )
		(*t).encoding = AUDIO_ENCODING_ALAW ;
	else if ( e == ADPCM_G722 )
		(*t).encoding = AUDIO_ENCODING_ADPCM ;
	else if (
		( e == LINEAR_8  ) || ( e == FIXED_8  ) ||
		( e == LINEAR_16 ) || ( e == FIXED_16 ) ||
		( e == LINEAR_24 ) || ( e == FIXED_24 ) ||
		( e == LINEAR_32 ) || ( e == FIXED_32 )
		)
		(*t).encoding = AUDIO_ENCODING_SLINEAR ;
	else
		ENOUGH
	SUCCESS

	N ( ioctl ( fd, AUDIO_SETINFO, &ai ) + 1 )
	*f = fd ;
FUNC_END
