#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sysexits.h>
#include <err.h>
#include "success.h"
#include "rational.h"
#include "sun_au.h"
#include "synth.h"
#include "parse.h"

#define MAX_DEFN_COUNT 24


int
main ( int argc, char ** argv )
{
	enum au_encoding encoding = LINEAR_16 ;
	int  samplerate           = 44100     ;
	int  channels             = 1         ;
	char * descr              = 0         ;
	char * file               = 0         ;

	int width  = 0 ;
	int height = 0 ;

	struct oscil_params_s ops ;
	struct         tone_s  ts ;
	struct rational wait ;

	sound_list_t sound_list ;

	int singular = 0 ;

	int i ;

	while ( ( i = getopt ( argc, argv, "e:r:c:d:f:w:h:s" ) ) != -1 )
		if (i == 'e')
			if (*optarg == 'f')
				encoding = FLOAT;
			else if (*optarg == 'd')
				encoding = DOUBLE;
			else if ( * optarg == '1' )
				encoding = LINEAR_8;
			else if ( * optarg == '2' )
				encoding = LINEAR_16;
			else if ( * optarg == '3' )
				encoding = LINEAR_24;
			else if ( * optarg == '4' )
				encoding = LINEAR_32;
			else
				err(1, "invaid encoding requested");
		else if (i == 'r')
			samplerate = atoi ( optarg ) ;
		else if (i == 'c')
			channels = atoi ( optarg ) ;
		else if (i == 'd')
			descr = optarg;
		else if (i == 'f')
			file = optarg;
		else if (i == 'w')
			width = atoi(optarg);
		else if (i == 'h')
			height = atoi(optarg);
		else if (i == 's')
			singular ++ ;
		else {
			fprintf ( stderr, "usage: %s\
\n  [-e <encoding>]\
\n  [-r <rate>]\
\n  [-c <channels>]\
\n  [-d <description>]\
\n  [-f <output>]\
\n  [-w <width>]\
\n  [-h <height>]\
\n  [-s]\n", * argv );
			return 1 ;
		}

	synth_init ( encoding, samplerate, channels, descr, file, width, height );
	defn = make_definitions_db ( MAX_DEFN_COUNT );

	cprs.p = 0 ; cprs.q = 0 ;
	odve.p = 1 ; odve.q = 1 ;

	ops.len.p = 1 ; ops.len.q =  1 ;
	ops.amp.p = 1 ; ops.amp.q =  1 ;
	ops.fad.p = 1 ; ops.fad.q = 16 ;
	ops.z = 1 ;

	ts.orig.p = -1 ; ts.orig.q =  1 ;
	ts.note.p =  0 ; ts.note.q = 12 ;

	wait.p = 1 ; wait.q = 1 ;

	sound_list = parse_sound ( &ops, &ts, &wait );
	sound_list = sound_list_reverse ( sound_list );
	if ( singular )
		play_sound_list ( sound_list );
	else
		while ( 0 == 0 )
			play_sound_list ( sound_list );
	synth_final ();

	return ( EX_OK ) ;
}
