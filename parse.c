#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sysexits.h>
#include <err.h>
#include "success.h"
#include "rational.h"
#include "sun_au.h"
#include "synth.h"
#include "parse.h"

#define LOOP while ( 0 == 0 )

defs_db_t
make_definitions_db ( u_int count )
{
	defs_db_t st ;

	st = malloc ( sizeof ( struct defs_db_s ) );
	st->count = 0 ;
	st->records = (struct definition_s *) calloc ( count,
		sizeof ( struct definition_s )
	);
	return st ;
}

struct definition_s *
find_record ( char label, defs_db_t d )
{
	u_int i ;
	u_int found ;

	/* FIXME: what if i >= MAX_DEFN_COUNT ? */
	found = d->count ;
	for ( i = 0 ; i < d->count ; i ++ )
		if ( d->records[i].label == label )
			{
				found = i ;
				break ;
			}
	if ( found == d->count ) {
		d->count ++ ;
		d->records[found].label = label ;
	}

	return d->records + found ;
}

int
is_defined ( char label, defs_db_t d )
{
	int i ;
	int found ;

	found = 0 ;
	for ( i = 0 ; i < d->count ; i ++ )
		if ( d->records[i].label == label )
			{
				found ++ ;
				break ;
			}

	return found ;
}

char
parse_rat(struct rational *r)
{
	char c;

	scanf("%d", &(r->p));
	scanf("%c", &c);
	if (c == ':') {
		scanf("%d", &(r->q));
		return getchar();
	} else
		return c;
}

char
parse_definition ( defs_db_t d )
{
	struct definition_s * record_p ;

	record_p = find_record ( getchar (), d );
	return parse_rat( &( record_p->note ) );
}

oscil_list_t
oscil_list_push ( list, tone, ops )
	oscil_list_t list ;
	struct rational tone ;
	struct oscil_params_s ops ;
{
	oscil_list_t result ;

	result = ( struct oscil_cell_s * ) malloc
		( sizeof ( struct oscil_cell_s ) );
	result->current.tone = tone ;
	result->current.ops = ops ;
	result->next = list ;

	return ( result );
}

oscil_list_t
oscil_list_concat ( first, last )
	oscil_list_t first, last ;
{
	oscil_list_t cell ;

	if ( first == NULL )
		return ( last );
	for ( cell = first ; cell->next ; cell = cell->next );
	cell->next = last ;
	return ( first );
}

sound_list_t
sound_list_push ( list, osc_list, wait )
	sound_list_t list ;
	oscil_list_t osc_list ;
	struct rational wait ;
{
	sound_list_t result ;

	result = ( struct sound_cell_s * ) malloc
		( sizeof ( struct sound_cell_s ) );
	result->current.oscillators = osc_list ;
	result->current.wait = wait ;
	result->next = list ;

	return ( result );
}

sound_list_t
sound_list_concat ( first, last )
	sound_list_t first, last ;
{
	sound_list_t cell ;

	if ( first == NULL )
		return ( last );
	for ( cell = first ; cell->next ; cell = cell->next );
	cell->next = last ;
	return ( first );
}

sound_list_t
sound_list_reverse ( list )
	sound_list_t list ;
{
	sound_list_t sl0 ;
	sound_list_t sl ;

	sl0 = NULL ;
	sl = list ;
	while ( sl != NULL )
		{
		sound_list_t t ;

		t = sl->next ;
		sl->next = sl0 ;
		sl0 = sl ;
		sl = t ;
		}

	return ( sl0 );
}

sound_list_t
sound_list_mix ( l0, l1 )
	sound_list_t l0, l1 ;
{
	sound_list_t first ;

	first = NULL ;
	LOOP
		{
		sound_list_t l ;
		struct rational wait0, wait1 ;

		if ( l0 == NULL )
			{
			first = sound_list_concat (
				sound_list_reverse ( l1 ), first );
			break ;
			}
		if ( l1 == NULL )
			{
			sound_list_t l ;

			l = l0 ;
			l0 = l1 ;
			l1 = l ;

			continue ;
			}
		l = ( struct sound_cell_s * ) malloc
			( sizeof ( struct sound_cell_s ) );

		l->current.oscillators = oscil_list_concat (
			l0->current.oscillators ,
			l1->current.oscillators );
		wait0 = l0->current.wait ;
		wait1 = l1->current.wait ;

		l0 = l0->next ;
		l1 = l1->next ;
		if ( rgt ( wait1, wait0 ) )
			{
			l->current.wait = wait0 ;
			l1 = sound_list_push ( l1, NULL, rsub ( wait1, wait0 ) );
			}
		else if ( rgt ( wait0, wait1 ) )
			{
			l->current.wait = wait1 ;
			l0 = sound_list_push ( l0, NULL, rsub ( wait0, wait1 ) );
			}
		else
			l->current.wait = wait1 ;

		l->next = first ;
		first = l ;
		}

	return ( first );
}

sound_list_t
parse_sound ( ops, ts, wait )
	struct oscil_params_s * ops ;
	struct         tone_s *  ts ;
	struct rational * wait ;
{
	sound_list_t sound_list ;
	sound_list_t sound_list_ ;

	/* parameters of second stream */
	struct oscil_params_s ops_ ;
	struct         tone_s  ts_ ;
	struct rational wait_ ;

	ops_ = * ops ;
	ts_  = * ts ;
	wait_ = * wait ;

	sound_list = NULL ;
	sound_list_ = NULL ;
	LOOP
	{
		oscil_list_t osc_list ;
		int i ;	/* for a character to be read */

		osc_list = NULL ;
		/* Recognized tokens: a c d f l m n o t x z , < > ( ) [ ] */
read_char:
		i = getchar();
		LOOP
		if ( is_defined ( i, defn ) ) {
			osc_list = oscil_list_push ( osc_list,
				rsum ( ts->orig, find_record ( i, defn ) -> note ),
				* ops );
			goto read_char ;
		}
		else if (i == 'l') i = parse_rat( &ops->len );
		else if (i == 'm') i = parse_rat( wait );
		else if (i == 't') i = parse_rat( wait ),
			ops->len = * wait ;
		else if (i == 'f') i = parse_rat( &ops->fad );
		else if (i == 'a') i = parse_rat( &ops->amp );
		else if (i == 'd') i = parse_definition( defn );
		else if (i == 'o') i = parse_rat( &odve );
		else if (i == 'c') i = parse_rat( &cprs );
		else if (i == 'n') {
			i = parse_rat( &ts->note );
			/* TODO: need to have separate command */
			osc_list = oscil_list_push ( osc_list,
				rsum ( ts->orig, ts->note ),
				* ops );
		}
		else if (i == '>')
			{
				ts->orig.p ++ ;
				goto read_char ;
			}
		else if (i == '<')
			{
				ts->orig.p -- ;
				goto read_char ;
			}
		else if (i == '(')
		{
			struct oscil_params_s op = * ops ;
			struct         tone_s  t = *  ts ;
			struct rational w = * wait ;

			sound_list = sound_list_concat (
				parse_sound ( &op, &t, &w ),
				sound_list );
			goto read_char ;
		}
		else if (i == ')') goto return_sound_list ;
		else if (i == '[')
			{
			sound_list_ = sound_list_concat (
				parse_sound ( &ops_, &ts_, &wait_ ),
				sound_list_ );
			goto read_char ;
			}
		else if (i == ']') goto return_sound_list ;
		else if (i == ',') break;
		else if ( i == 'x' || i == 0 || i + 1 == 0 )
			goto return_sound_list ;
		else if (i == 'z')
			{
				scanf( "%u",  &ops->z );
				goto read_char ;
			}
		else goto read_char ; /* ignore current, read next */

		sound_list = sound_list_push ( sound_list, osc_list, * wait );
	}
return_sound_list:
	return ( sound_list_mix (
		sound_list_reverse ( sound_list  ),
		sound_list_reverse ( sound_list_ )
		) );
}

void
play_sound_list ( list )
	sound_list_t list ;
{
	sound_list_t sl ;

	for ( sl = list ;
		sl ; sl = sl->next )
		{
		oscil_list_t ol ;

		for ( ol = sl->current.oscillators ;
				ol ; ol = ol->next )
			osc_set ( ol->current );
		play_sound ( sl->current.wait );
		}
}
