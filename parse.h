struct tone_s {
	struct rational
		orig , /* origin (octave) */
		note ; /* n-th part of octave */
};


struct oscil_cell_s {
	struct oscil_s current ;
	struct oscil_cell_s * next ;
};

struct sound_s {
	struct oscil_cell_s * oscillators ;
	struct rational wait ;
};

struct sound_cell_s {
	struct sound_s current ;
	struct sound_cell_s * next ;
};


struct definition_s {
	char label ;
	struct rational note ;
};

struct defs_db_s {
	u_int count ;
	struct definition_s * records ;
};

typedef struct defs_db_s * defs_db_t ;
typedef struct oscil_cell_s * oscil_list_t ;
typedef struct sound_cell_s * sound_list_t ;

defs_db_t defn ;

defs_db_t
make_definitions_db ( u_int );

sound_list_t
parse_sound (
	struct oscil_params_s * ,
	struct         tone_s * ,
	struct       rational * );

void
play_sound_list ( sound_list_t );

sound_list_t
sound_list_reverse ( sound_list_t );
