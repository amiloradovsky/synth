#include <stdio.h>
#include <math.h>
#include "rational.h"

double
rtod(struct rational a)
{
	return ((double)a.p / (double)a.q);
}

inline struct rational
rnew(int a, int b)
{
	struct rational r;

	r.p = a;
	r.q = b;

	return r;
}

rational_base_t
gcd ( a, b )
	rational_base_t a, b ;
{
	rational_base_t m = a % b ;  /* a = d * b + m  (m < b) */

	if ( m == 0 )
		return ( b );
	else
		return ( gcd ( b, m ) );
}

rational_base_t
lcd ( a, b )
	rational_base_t a, b ;
{
	return ( a / gcd ( a, b ) * b );
}

/* a.p / a.q + b.p / b.q = (a.p * b.q +/- b.p * a.q) / (a.q * b.q) =
 = (a.p * (b.q / gcd (a.q, b.q)) +/- b.p * (a.q / gcd (a.q, b.q)))
   / lcd (a.q, b.q) */

/* divide first, then multiply; return irreducible */

inline struct rational
rsum ( a, b )
	struct rational a, b ;
{
	struct rational r, s ;
	rational_base_t g, gr ;

	g = gcd ( a.q, b.q );
	r.p = (b.q / g) * a.p + (a.q / g) * b.p ;
	r.q = a.q / g * b.q ; /* lcd */

	gr = gcd ( r.p, r.q );
	s.p = r.p / gr ;
	s.q = r.q / gr ;

	return s ;
}

inline struct rational
rsub ( a, b )
	struct rational a, b ;
{
	struct rational r, s ;
	rational_base_t g, gr ;

	g = gcd ( a.q, b.q );
	r.p = (b.q / g) * a.p - (a.q / g) * b.p ;
	r.q = a.q / g * b.q ; /* lcd */

	gr = gcd ( r.p, r.q );
	s.p = r.p / gr ;
	s.q = r.q / gr ;

	return s ;
}

inline struct rational
rmul ( a, b )
	struct rational a, b ;
{
	struct rational r, a_, b_ ;
	rational_base_t g0, g1 ;

	g0 = gcd ( a.p, b.q );
	g1 = gcd ( a.q, b.p );
	a_.p = a.p / g0 ;
	a_.q = a.q / g1 ;
	b_.p = b.p / g1 ;
	b_.q = b.q / g0 ;
	r.p = a_.p * b_.p;
	r.q = a_.q * b_.q;

	return r;
}

int
rgt ( a, b )
	struct rational a, b ;
{
	struct rational c ;

	c = rsub ( a, b );

	return ( c.p * c.q > 0 );
}
