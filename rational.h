typedef int rational_base_t ;

struct rational {
	rational_base_t p, q ;
};

inline double
rtod(struct rational);

inline struct rational
rnew ( rational_base_t, rational_base_t );

inline struct rational
rsum(struct rational, struct rational);

inline struct rational
rsub(struct rational, struct rational);

inline struct rational
rmul(struct rational, struct rational);

int
rgt ( struct rational, struct rational );

/*
inline struct rational
cexp(struct rational);
*/
