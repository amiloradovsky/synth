/* Checking for success of function calls and logical constraints */

typedef unsigned int success_t ;

#define FUNC	success_t
#define FUNC_BEGIN	{ unsigned int check_point = 1 ;
#define FUNC_END	return 0 ; }

/* Logical conditions */
#define SUCCESS	check_point ++ ;
#define ENOUGH	return check_point ;

/* Checking of function status which must be Z (zero) or N (non-zero) */
#define Z(X) if ( X ) ENOUGH else SUCCESS
#define N(X) Z ( ! ( X ) )
