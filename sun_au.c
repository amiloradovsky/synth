#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <err.h>
#include "success.h"
#include "sun_au.h"
#include "buffer.h"

const char * au_magic = ".snd" ;

const int au_sample_size_v[28] = {
	0, 1,       /* none, u-law */
	1, 2, 3, 4, /* signed linear */
	4, 8,       /* floating point */
	0, 0, 0,	/* unknown size */
	1, 2, 3, 4, /* fixed point */
	0, 0, 0,    /* unknown size */
	2, 2, 2,    /* linear with emphasis and\or compression */
	0,          /* unknown */
	0, 0, 0, 0, 0, /* ADPCM */
	1           /* a-law */
};

FUNC au_header_read ( f, h, d )
	int f ;
	struct au_header * h ;
	char ** d ;
FUNC_BEGIN
	int l, s, t ;

	t = sizeof ( (*h).magic );	/* first field, all other has same size */
	s = sizeof ( *h ) / t ;

	/* header itself */
	Z ( au_buffer_read ( f, ( int8_t * ) h, &s, t ) )
	if ( htobe32( (*h).magic ) != *( (u_int32_t *) au_magic ) ) {
		fprintf ( stderr, "Isn't .AU file.\n" );
		ENOUGH
	}

	/* file description */
	l = (*h).offset - sizeof ( *h ) ;
	if ( l ) {
		*d = malloc ( l );
		Z ( buffer_rw ( READ, f, *( (int8_t **) d), &l, 1 ) )
	} else
		*d = 0 ;
FUNC_END

FUNC au_header_write ( f, e, r, c, n, d )
	int f ;	/* descriptor */
	enum au_encoding e ;
	int r, c ;	/* samples per second, channels */
	int n ;	/* total count of samples in file */
	const char * d ; /* file description */
FUNC_BEGIN
	int l, t, s ;
	struct au_header h ;

	t = sizeof ( h.magic );	/* first field, all other has same size */
	s = sizeof ( h ) / t ;
	l = d ? strlen ( d ) : 0 ;

	/* header itself */
	h.magic = be32toh( *( (u_int32_t *) au_magic ) );
	h.encoding = e ;
	h.sample_rate = r ;
	h.channels = c ;
	h.offset = sizeof ( h ) + l ;
	if ( n && au_sample_size_v [ e ] )
		h.data_size = n * au_sample_size_v [ e ];
	else
		h.data_size = ( unsigned ) -1 ;	/* unknown size */
	Z ( au_buffer_write ( f, ( int8_t * ) &h, &s, t ) )

	/* file description */
	Z ( buffer_rw ( WRITE, f, ( int8_t * ) d, &l, 1 ) )
FUNC_END

FUNC au_buffer_read ( f, b, c, r )
	int f ;	/* descriptor */
	int8_t * b ;	/* buffer */
	int * c ;	/* count of samples */
	int r ;
{
	int s ;

	s = buffer_rw ( READ, f, b, c, r );
#ifdef LITTLE_ENDIAN_MACHINE
	/* Aactually here is lack of success check */
	if ( buffer_revert ( b, *c, r ) )
		s ++ ;
#endif
	return s ;
}

FUNC au_buffer_write ( f, b, c, r )
	int8_t * b ;	/* buffer */
	int f ;	/* descriptor */
	int * c ;	/* count of samples */
	int r ;
{
	int s ;

	s = 0 ;
#ifdef LITTLE_ENDIAN_MACHINE
	/* Aactually here is lack of success check too */
	if ( buffer_revert ( b, *c, r ) )
		s ++ ;
#endif
	s += buffer_rw ( WRITE, f, b, c, r );

	return s ;
}
