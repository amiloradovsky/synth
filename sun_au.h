#include <sys/types.h>

const int au_sample_size_v[28];

struct au_header {
	u_int32_t magic;       /* magic number (.snd) */
	u_int32_t offset;      /* byte offset to start of audio data */
	u_int32_t data_size;   /* data length in bytes */
	u_int32_t encoding;    /* data encoding */
	u_int32_t sample_rate; /* samples per second */
	u_int32_t channels;    /* number of interleaved channels */
};

enum au_encoding {
	ULAW         = 1,  /* 8-bit u-law */
	LINEAR_8     = 2,  /* 8-bit linear PCM */
	LINEAR_16    = 3,  /* 16-bit linear PCM */
	LINEAR_24    = 4,  /* 24-bit linear PCM */
	LINEAR_32    = 5,  /* 32-bit linear PCM */
	FLOAT        = 6,  /* 32-bit IEEE floating point */
	DOUBLE       = 7,  /* 64-bit IEEE double precision float */
	FRAGMENTED   = 8,  /* Fragmented sample data */
	DSP          = 10, /* DSP program */
	FIXED_8      = 11, /* 8-bit fixed point */
	FIXED_16     = 12, /* 16-bit fixed point */
	FIXED_24     = 13, /* 24-bit fixed point */
	FIXED_32     = 14, /* 32-bit fixed point */
	EMPHASIS     = 18, /* 16-bit linear with emphasis */
	COMPRESSED   = 19, /* 16-bit linear compressed */
	EMP_COMP     = 20, /* 16-bit linear with emphasis and compression */
	MUSIC_KIT    = 21, /* Music kit DSP commands */
	ADPCM_G721   = 23, /* 4-bit CCITT G.721 ADPCM */
	ADPCM_G722   = 24, /* CCITT G.722 ADPCM */
	ADPCM_G723_3 = 25, /* CCITT G.723.3 ADPCM */
	ADPCM_G723_5 = 26, /* CCITT G.723.5 ADPCM */
	ALAW         = 27  /* 8-bit A-law G.711 */
};

FUNC au_header_read ( int, struct au_header *, char ** );

FUNC au_header_write ( int, enum au_encoding, int, int, int, const char * );

FUNC au_buffer_read ( int, int8_t *, int *, int );

FUNC au_buffer_write ( int, int8_t *, int *, int );
