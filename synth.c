#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <err.h>
#include "success.h"
#include "complex.h"
#include "rational.h"
#include "video.h"
#include "sun_au.h"
#include "audio.h"
#include "synth.h"

#define MAX_OSCILS 256
#define TONOMETER  440	/* note A of 1-st octave */

struct oscillator {
	struct complex st;   /* current state */
	struct complex ekdt; /* exponent differential */
	unsigned int n;	     /* count of times to be iterated */
};

struct oscillator oscil_v[MAX_OSCILS];
int rate, channels ;
int plotting;
REAL dt ;	/* 2pi * sample_length */

OUTPUT
iterate(struct oscillator v[])
{
	int i;
	REAL r ;             /* sines' superposition */

	r = 0;
	for (i = 0; i < MAX_OSCILS; i++)
		if (v[i].n) {
			v[i].st = cmul(v[i].st, v[i].ekdt);
			r += v[i].st.re;
			if (v[i].n == 1) {
				struct complex t ;

				t = cmul ( v[i].st, v[i].ekdt );
				if ( ( v[i].st.re < 0 && t.re > 0 )
				  || ( v[i].st.re > 0 && t.re < 0 ) ) {
					v[i].n = 0 ;
				};
			} else
				v[i].n--;
		}
	return r;
}

/* set yet one oscillator (sine source) to be evaluated */
int
osc_add ( v, freq, fading, amp, n )
	struct oscillator *v;
	REAL freq, fading, amp ;
	uint n;
{
  /* pre-compute the parameters for the new oscillator */
  struct complex init_state = cnew ( 0, amp );
  struct complex time_step  = cnew ( dt, 0 );
  struct complex scale_rate = cnew (- fading, freq );
  struct complex multiplier = cexp_ ( cmul ( scale_rate, time_step ) );

  /* seek for a free one and activate it */
  static int i = 0 ;  /* next free, presumably */

  int i0 = i ;  /* store */
  do

    if ( v[i].n == 0 )  /* found a free */
      {
        v[i].st = init_state ;
        v[i].ekdt = multiplier ;
        v[i].n = n ;  /* reset it */
        if ( ++ i == MAX_OSCILS ) i = 0 ;
        return 0 ;  /* Ok, done */
      }

  while ( i != i0 );
  /* nothing found, panic */

  return 1 ;
}

int
osc_set ( os )
	struct oscil_s os ;
{
	double f, a, t ;
	int i ;

	f = rtod ( os.ops.fad );
	a = rtod ( os.ops.amp );
	t = TONOMETER * pow ( 2, rtod ( os.tone ) );	/* base tone */

	/* overtones (all of equal amplitude) */
	for ( i = 1 ; i <= os.ops.z ; i ++ )
		osc_add ( oscil_v, t * i, f * i * i, a,
			rate * os.ops.len.p / os.ops.len.q );

	return ( 0 );
}

int
overdrive ( x, d )
	OUTPUT * x ;
	REAL d ;
{
	/* d may be non-positive too, but only the absolute matters */
	if ( fabs ( *x ) > fabs ( d ) ) {
		/* x is never zero here */
		if ( ( *x > 0 && d > 0 ) || ( *x < 0 && d < 0 ) ) {
			*x = d ;
		} else
			*x = - d ;
	}

	return ( 0 );
}

int
compression ( x, k )
	OUTPUT * x ;
	REAL k ;
{
	REAL t = k * ( 1 - exp ( - fabs ( *x ) ) );
	*x = *x > 0 ? t : - t ;

	return ( 0 );
}

FUNC play_sample ( OUTPUT x )
FUNC_BEGIN
	u_int j ;

	/* applying the effects */
	if ( odve.q != 1 || odve.p != 1 )
		overdrive ( &x, rtod ( odve ) );
	if ( cprs.q && cprs.p )
		compression ( &x, rtod ( cprs ) );

	/* output format's specific stuff */
	for ( j = 0 ; j < channels ; j ++ )
		Z ( sample_push ( x ) )

	if ( plotting )
		point_push ( x );
FUNC_END

/* rendering of settings                        *
 * (put time valued function to output buffers) */
FUNC play_sound ( wait )
	struct rational wait ;
FUNC_BEGIN
	OUTPUT x ;
	u_int i ;

	for ( i = rate * rtod( wait ); i ; i -- )
	{
		x = iterate(oscil_v);
		Z( play_sample ( x ) )
	}
FUNC_END

int
synth_init ( e, r, c, d, f, w,h )
	enum au_encoding e ;
	int r, c ;	/* samples per second, channels */
	u_int w, h ;	/* width, height */
	char * d, * f ;	/* description, file name */
{
	if ( audio_init ( e, r, c, d, f, f ? FILE_OUTPUT : DEVICE_OUTPUT ) )
		err ( 1, "Initializing audio output.\n" );
	plotting = ! window_init ( w, h );
	memset(oscil_v, 0, MAX_OSCILS * sizeof(struct oscillator));
	channels = c ;
	rate = r ;
	dt = 2 * clog_ ( cnew ( -1, 0 ) ).im / rate ;

	return ( 0 ) ;
}

void
synth_final ()
{
	buffer_flush ();
}
