struct oscil_params_s {
	struct rational
		len , /* sound length (sec.) */
		amp , /* linear scale */
		fad ; /* damping rate */
	u_int z ;		/* count of overtones */
};

struct oscil_s {
	struct oscil_params_s ops ;
	struct rational tone ;
};

struct rational
	cprs ,	/* compression (exponential scale) */
	odve ;	/* overdrive (max. ampl.) */

int synth_init ( enum au_encoding, int, int,
	char *, char *, u_int,u_int );
void synth_final ();

int osc_set ( struct oscil_s );
FUNC play_sound ( struct rational );
