#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <err.h>
#include <X11/Xutil.h>
#include "video.h"

#define COLORS_COUNT	3
#define DPY_W(dpy)	DisplayWidth(dpy, DefaultScreen(dpy))
#define DPY_H(dpy)	DisplayHeight(dpy, DefaultScreen(dpy))

Display *dpy;
Window win;
GC gc ;
float * buffers [ COLORS_COUNT ];
u_long  colors  [ COLORS_COUNT ] = { 0x000000, 0xBBBBBB, 0xFFFFFF };
int f_carrage;
u_int width, height;

int
window_init ( w, h )
	u_int w, h ;
{
	int i ;
	XSetWindowAttributes a;

	if (!(dpy = XOpenDisplay(NULL)))
		return 1 ;

	if ( w || h ) {
		width  = w ? ( w < DPY_W ( dpy ) ? w : DPY_W ( dpy ) ) : h ;
		height = h ? ( h < DPY_H ( dpy ) ? h : DPY_H ( dpy ) ) : w ;
	} else
		return 2 ;

	for ( i = 0 ; i < COLORS_COUNT ; i ++ ) {
		*( buffers + i ) = (float *) calloc ( width, sizeof ( float ) );
		bzero ( *( buffers + i ), width * sizeof ( float ) );
	}
	f_carrage = 0;

	bzero ( &a, sizeof ( a ) );
	a.event_mask = KeyPressMask|KeyReleaseMask|ExposureMask;
	if (!(win = XCreateWindow(dpy, DefaultRootWindow(dpy), 0,0 ,
			width, height, 1, CopyFromParent, InputOutput,
			CopyFromParent, CWEventMask, &a)))
		return 3 ;

	if ( ! XMapWindow (dpy, win) )
		return 4 ;
	gc = DefaultGC ( dpy, DefaultScreen ( dpy ) );

	return 0 ;
}

void
points_draw()
{
	int i, j ;

	/* drawing of point on the window */
	/* origin of diagram: (0, height / 2) */
	for ( i = 1 ; i < width ; i++ )
		for ( j = 0 ; j < COLORS_COUNT ; j ++ ) {
			XSetForeground ( dpy, gc, *( colors + j ) );
			XDrawLine ( dpy, win, gc,
				i - 1, (1.0 + *(*(buffers + j) + i - 1)) * height / 2,
				i    , (1.0 + *(*(buffers + j) + i    )) * height / 2
			);
		}
	for ( i = 0; i < width; i++ )
		for ( j = 1 ; j < COLORS_COUNT ; j ++ )
			*( *( buffers + j - 1 ) + i ) = *( *( buffers + j ) + i );
}

void
point_push ( float x )
{
	XEvent ev;

	while ( XCheckWindowEvent ( dpy, win, ExposureMask, &ev ) == True ) {
		XSetForeground ( dpy, gc, * colors );
		XFillRectangle ( dpy, win, gc,
			ev.xexpose.x, ev.xexpose.y,
			ev.xexpose.width, ev.xexpose.height );
	}
	*( *( buffers + COLORS_COUNT - 1 ) + f_carrage++ ) = x;
	if ( f_carrage == width ) {
		f_carrage = 0 ;
		points_draw ();
	}
}
